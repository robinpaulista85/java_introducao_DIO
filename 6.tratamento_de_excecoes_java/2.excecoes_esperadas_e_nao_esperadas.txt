                CHECKED EXCEPTIONS 

São exceções esperadas, cujo fluxo ou método de um sistema foi preparado para 
receber. Um bom exemplo é uma exceção d enegócio, onde se deseja informar um erro 
caso a exceção esperada ocorra. 

Exemplo: 

try {
  PreparedStatement stmt = con.prepareStatement(query);
  // ...
} catch (SQLException e) {

  throw new AcessoADadosException("Problema na criação do Statement",  e);

}

                UNCHECKED EXCEPTIONS 

São exceções não esperadas para o fluxo ou método de um sistema, um bom exemplo 
é a famosa NullPointException que ocorre quando se tenta acessar uma referência
de memória vazia, ou recuperar uma instância que não existe, dentre outros 
motivos.

Exemplo: 

try{
    CarroVo carro = new CarroVo();
    carro.getPlaca();
} catch (IntegrationException e){
    throw new BusinessException("Erro na criação do objeto", e);

}

                BLOCO TRY CATCH 

O bloco try catch sempre é utilizado quando no prcoesso que será executado dentro
de um método é esperado um erro, então cria-se um bloco "protegido" onde qualquer
erro que ocorra dentro do trecho "try" é direcionado para o trecho "catch" e
sofrerá o devido tratamento de erro. 

Exemplo:

try {
  PreparedStatement stmt = con.prepareStatement(query);
  // ...
} catch (SQLException e) {

  throw new AcessoADadosException("Problema na criação do Statement",  e);

}

